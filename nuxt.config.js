const core = require('./package.json')

module.exports = {
  dev: (process.env.NODE_ENV !== 'production'),
  srcDir: './src',
  /*
  ** Headers of the page
  */
  head: {
    title: 'workspace_name',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js + Vuetify.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  modules: [
    ['@nuxtjs/vuetify', {
      css: false,
      materialIcons: true,
      theme: {
        primary: '#FFC52A',
        accent: '#140254',
        secondary: '#424242',
        info: '#0D47A1',
        warning: '#ffb300',
        error: '#B71C1C',
        success: '#2E7D32'
      },
      options: {
        minifyTheme: val => {
          return process.env.NODE_ENV === 'production'
            ? val.replace(/[\s|\r\n|\r|\n]/g, '')
            : val
        }
      }
    }],
    /**
     * Google Analyticsの有効化
     * @see https://github.com/nuxt-community/analytics-module
     */
    /*
    ['@nuxtjs/google-analytics', {
      id: 'UA-12301-2'
    }],
    */
    /**
     * キャッシュの有効化
     * @see https://ja.nuxtjs.org/faq/cached-components
     */
    /*
    ['@nuxtjs/component-cache', {
      max: 10000,
      maxAge: 1000 * 60 * 60
    }],
    */
    /**
     * サイトマップ作成
     * @see https://github.com/nuxt-community/sitemap-module
     */
    /*
    ['@nuxtjs/sitemap', {
      path: '/sitemap.xml',
      hostname: 'https://example.com',
      cacheTime: 1000 * 60 * 15,
      gzip: true,
      generate: false, // Enable me when using nuxt generate
      exclude: [
        '/secret',
        '/admin/**'
      ],
      routes: [ // 関数も可能
        '/page/1',
        {
          url: '/page/2',
          changefreq: 'daily',
          priority: 1,
          lastmodISO: '2017-06-30T13:30:00.000Z'
        }
      ]
    }],
    */
    /**
     * Three different feed types (RSS 2.0, ATOM 1.0 and JSON 1.0)
     * @see https://github.com/nuxt-community/feed-module
     */
    /*
    ['@nuxtjs/feed', [
      // A default feed configuration object
      {
        path: '/feed.xml', // The route to your feed.
        async create (feed) {}, // The create function (see below)
        cacheTime: 1000 * 60 * 15, // How long should the feed be cached
        type: 'rss2' // Can be: rss2, atom1, json1
      }
    ]],
    */
    /**
     * Add Google Tag Manager
     * @see https://github.com/nuxt-community/modules/tree/master/packages/google-tag-manager
     * @todo Required Should be in form of GTM-XXXXXXX
     */
    ['@nuxtjs/google-tag-manager', { id: 'GTM-PDNHRNQ' }],
    /**
     * Responsive Touch Compatible Toast
     * @see https://github.com/nuxt-community/modules/tree/master/packages/toast
     */
    /*
    ['@nuxtjs/toast', {
      position: 'top-center'
    }],
    */
    // ['@nuxtjs/auth'],
    // ['@nuxtjs/axios'],
    '~/socket.io'
  ],
  plugins: [],
  css: [
    '~/assets/style/app.styl'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    babel: {
      plugins: ['transform-decorators-legacy', 'transform-class-properties']
    },
    vendor: ['vuedraggable', 'underscore'],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      config.module.rules = config.module.rules.map(rule => {
        if (rule.loader === 'url-loader' && rule.test.toString().includes('svg')) {
          return {
            ...rule,
            test: /\.(png|jpe?g|gif)$/
          }
        }
        return rule
      })

      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader'
      })

      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
