const pack = require('./package.json')
module.exports = {
  // ドキュメント生成元
  source: './src',
  // ドキュメント生成先
  destination: './docs',
  // プラグイン
  plugins: [
    {
      name: 'esdoc-standard-plugin',
      option: {
        // lint
        lint: { enable: true },
        // coverage
        coverage: {
          enable: true,
          kind: ["class", "method", "member", "get", "set", "constructor", "function", "variable"]
        },
        // accessor
        accessor: { access: ["public", "protected", "private"], autoPrivate: true},
        // undocument
        undocumentIdentifier: { enable: true },
        // unexported
        unexportedIdentifier: { enable: false },
        // typeInterface
        typeInference: { enable: true },
        // ブランド設定
        brand: {
          logo: './src/static/v.png',
          title: pack.name,
          description: pack.description,
          repository: pack.repository,
          site: pack.site,
          author: pack.author,
          image: './src/static/logo.png'
        },
        // テストフォルダ
        test: {
          source: './test'
        },
      }
    },
    { name: 'esdoc-node'},
    { name: 'esdoc-ecmascript-proposal-plugin', option: { all: true} },
    {
      name: './.esdoc-manual.js',
      option: {
        globalIndex: true,
        index: './README.md',
        asset: './src/static',
        files: [
          './src/assets/README.md',
          './src/components/README.md',
          './src/layouts/README.md',
          './src/middleware/README.md',
          './src/pages/README.md',
          './src/plugins/README.md',
          './src/socket.io/README.md',
          './src/socket.io/ffmpeg/README.md',
          './src/static/README.md',
          './src/store/README.md'
        ]
      }
    }
  ]
}
