# Leed

![node version](https://img.shields.io/badge/node-8.11.3-green.svg)

> 広告動画の作成をアシストするサービス

- 動画を自動で広告サービスに登録（未実装）
- 販促データをもとにオススメテンプレートを推薦（未実装）

## ターゲット

- マーケティング担当、広報
- 予算を動かせる経営者。
- オウンドメディアを運営している会社。

## Build Setup

``` bash
# document
$ npm run doc

# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
```