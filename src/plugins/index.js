/**
 * @external {NUXT.context} https://ja.nuxtjs.org/api/context/
 */
/**
 * @external {NUXT.inject} https://ja.nuxtjs.org/guide/plugins/#アプリケーションのルートや-context-に挿入する
 */
/**
 * @external {Vue} https://jp.vuejs.org/v2/guide/instance.html
 */
/**
 * @external {Vue.instance} https://jp.vuejs.org/v2/api/#インスタンスプロパティ
 */
/**
 * @external {Router} https://router.vuejs.org/ja/api/#ルーターインスタンスプロパティ
 */
/**
 * @external {Router.$route} https://router.vuejs.org/ja/api/#ルートオブジェクト
 */
