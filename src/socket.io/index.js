const path = require('path')
const Handler = require('./handler')
/**
 * @external {Http.Request} http://expressjs.com/ja/api.html#req
 */
/**
 * @external {Http.Response} http://expressjs.com/ja/api.html#res
 */
/**
 * @external {Middleware.callback} http://expressjs.com/ja/api.html#middleware-callback-function-examples
 */
/**
 * @external {IO.Socket} https://socket.io/docs/server-api/#Socket
 */
/**
 * @external {IO.Server} https://socket.io/docs/server-api/#Server
 */
/**
 * @external {QueueObject} https://caolan.github.io/async/docs.html#QueueObject
 */

/**
 * socket.io対応モジュール
 * ``` node
 * // in nuxt.config.js
 * // export default / module.exports
 * module.exports = {
 *   modules: ['~/socket.io']
 * }
 * ```
 * @private
 * @param {Object} moduleOptions - モジュールオプション
 * @see https://github.com/nuxt/nuxt.js/blob/dev/lib/core/nuxt.js
 * @see https://ja.nuxtjs.org/api/internals-nuxt
 * @since 0.0.1
 * @version 0.0.1
 */
module.exports = function socketModule (moduleOptions) {
  // const options = Object.assign({ /* デフォルトのオプション */ }, this.options.io, moduleOptions)
  this.addServerMiddleware(Handler.getInstance)

  this.addVendor('socket.io-client')
  // 非クライアント時でも$socket.emitを受け取れるようにするため、{ ssr: false }にはしない。
  this.addPlugin({ src: path.resolve(__dirname, 'plugin.js') })
}
