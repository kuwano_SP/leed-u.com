import Vue from 'vue'
import IO, { Socket } from 'socket.io-client'

/**
 * socket.emitをPromiseに対応させる。
 *
 * @param {...Any} args - args[0]はイベント名
 * @return {Promise<any, Error>} self
 * @example
 * // in pages/index.js
 * some_action () {
 *   this.$socket.do('EVENT', ...payload).then(data => {
 *     // do something...
 *   })
 * }
 * @since 0.0.1
 * @version 0.0.1
 * @todo priorityに適正値を割り当てる
 */
export const emitAsync = function (...args) {
  // @todo とりあえず10で対応。
  const priority = 10
  args.push(priority)

  return new Promise((resolve, reject) => {
    args.push((err, data) => err ? reject(err) : resolve(data))
    this.emit.apply(this, args)
  }).catch(msg => Promise.reject(new Error(msg)))
}

/**
 * socket.ioクライアント接続用
 *
 * @param {NUXT.context} ctx - コンテキスト
 * @param {NUXT.inject} inject - インジェクト関数
 * @private
 * @since 0.0.1
 * @version 0.0.1
 */
export default function socketPlugin ({ app }, inject) {
  inject('socket', process.server ? Vue.prototype.$socket : IO.connect())

  if (process.server) return
  app.$socket.on('connect', () => {
    console.log('connected')
  })
  app.$socket.do = emitAsync
}
