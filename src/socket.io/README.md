# socket.io モジュール

サーバーとのソケット通信を可能にする。

## Usage

> in nuxt.config.js

```node
module.exports = {
  modules: ['~/socket.io']
}
```

### モジュール

`index.js`に定義。

### プラグイン

`plugin.js`に定義。

### ハンドラ

`handler.js`に定義。ffmpegを介する処理は別クラス`ffmpeg/index.js`に定義。