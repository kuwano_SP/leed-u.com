const consola = require('consola')
const logger = consola.withScope('socket.io:handle')
const assert = require('assert')
const { listen } = require('socket.io')
const config = {
  pingTimeout: 1000 * 60 * 30, // ms
  interval: 1000 * 3 // ms
}
const managerConfig = {
  baseURL: 'https://api.storyblok.com/v1/spaces/44760/',
  timeout: 3000,
  headers: {
    Authorization: 'yjEJrN0od4RwrEo9yCkULgtt-42986-zzX5aZRxwdQNsM_DtQyc'
  }
}

const cdnConfig = {
  method: 'GET',
  baseURL: 'https://api.storyblok.com/v1/cdn/',
  timeout: 3000,
  paramsSerializer (params) {
    params.token = 'SvLZcV7RhRHfJ4XtAR7akAtt'
    if (!params.cv) params.cv = new Date().getTime()
    return require('qs').stringify(params, {arrayFormat: 'brackets'})
  }
}

/**
 * socket.ioに関する処理をまとめたクラス
 *
 * ## イベントの受け取り
 *
 * '_'から始まらないメソッドはイベントリスナに登録
 *
 * @since 0.0.1
 * @version 0.0.1
 */
module.exports = class SocketHandler {
  /**
   * シングルトンの取得
   * @param {?Http.Request} req - サーバリクエスト
   * @param {?Http.Response} res - サーバレスポンス
   * @param {?function(): null} next - 処理を次に渡す。
   * @return {SocketHandler} インスタンス
   * @since 0.0.1
   * @version 0.0.1
   */
  static getInstance (req, res, next) {
    if (!SocketHandler.instance) {
      const server = res.connection.server
      if (!server) throw new Error('serverは必須です。')
      const io = listen(server, config)
      SocketHandler.instance = new SocketHandler(io)
      require('vue').prototype.$socket = SocketHandler
    }
    if (next) next()
    return SocketHandler.instance
  }

  /**
   * enqueuedの返り値であるPromiseが完了したらcallbackを発火する。
   * @param {function(): Promise<Any, Error>} enqueued -
   * @param {function(err: ?Error, result: ?Any): undefined} callback -
   * @example
   * // 正常時
   * callback(null, result)
   * // エラーが起こった時
   * callback(err)
   * @see https://caolan.github.io/async/global.html#AsyncFunction
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  static worker (enqueued, callback) {
    if (typeof enqueued !== 'function') throw new Error(`Invalid enqueued value passed. ${typeof enqueued}`)
    const queue = enqueued()
    if (!(queue instanceof Promise)) throw new Error(`Invalid queue type ${typeof queue}. queue mustbe promise.`)
    queue.then(result => callback(null, result)).catch(err => {
      logger.error(err)
      callback(err)
    })
  }

  /**
   * サーバー用emit関数
   * @param {String} event - 呼び出されたイベントの名前
   * @param {...any} args - 引数
   * @return {Promise<Any, Error>}
   * @since 0.0.1
   * @version 0.0.1
   */
  static do (event, ...args) {
    const $ = SocketHandler.instance
    if (typeof $[event] !== 'function')
      return Promise.reject(new Error(`method[${event}] not found.`))
    return $[event].apply($, args)
  }

  /**
   * コンストラクタ
   *
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  constructor (io) {
    /**
     * @type {Object} storyblok API処理
     * @property {axios} manager
     * @property {axios} cdn
     * @property {function(path: String, params: Object): axios} get
     * @property {function(path: String, data: Object): axios} post
     * @property {function(res: axios.Response): Promise<>} onSuccess
     * @property {function(err: Error): Promise<Error>} onFail
     * @private
     * @since 0.0.1
     * @version 0.0.1
     */
    this.$storyapi = {
      /**
       * MANAGEMENT API
       */
      manager: require('axios').create(managerConfig),
      /**
       * CONTENT DELIVERY API
       */
      cdn: require('axios').create(cdnConfig),
      /**
       *
       */
      get (path, params) {
        if (typeof params !== 'object') params = {}
        const apiType = 'version' in params ? 'cdn' : 'manager'
        return this[apiType].get(path, { params })
          .then(this.onSuccess, this.onFail)
      },
      /**
       *
       */
      post (path, data) {
        return this.manager.post(path, data)
          .then(this.onSuccess, this.onFail)
      },
      /**
       *
       */
      onSuccess (res) {
        const data = res.data || {}
        data.statusText = res.statusText
        // console.log(data) // for debug
        return Promise.resolve(data)
      },
      /**
       *
       */
      onFail (err) {
        logger.error(err.message)
        console.error(err.config || err)
        return Promise.reject(err)
      }
    }
    /**
     * @type {IO.Server} socket.io
     * @private
     * @since 0.0.1
     * @version 0.0.1
     */
    this.io = io
    /**
     * @type {IO.Socket}
     * @private
     * @since 0.0.1
     * @version 0.0.1
     */
    this.$socket = null
    /**
     * サーバーの処理キュー
     * @type {QueueObject}
     * @private
     * @since 0.0.1
     * @version 0.0.1
     */
    this.queue = require('async').priorityQueue(SocketHandler.worker, 1)

    this.io.sockets.on('connect', this._init)
  }

  /**
   * 接続確立時
   * @param {IO.Socket} socket - ソケット
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  _init (socket) {
    const $ = SocketHandler.instance
    $.$socket = socket
    const proto = Object.getPrototypeOf($)
    const methods = Object.getOwnPropertyNames(proto)

    for (const method of methods) {
      // メソッド名によっては反応せず
      if (method === 'constructor' || method.indexOf('_') === 0) continue
      socket.on(method, $._addListener(method))
    }
    logger.info(`socket has connected: ${socket.id}`)
    require('./ffmpeg').getInstance(socket.id)
  }

  /**
   * 指定されたメソッドのソケットリスナを登録する関数を返す。
   * 
   * @param {String} method - メソッド名
   * @return {function(): null} リスナーを登録する関数
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  _addListener (method) {
    return (...args) => {
      const _cb = args.pop()
      const $ = this
      const cb = function () {
        logger.info(`done: ${method}(${$.$socket.id})[${$.queue.running()}/${$.queue.length()}]`)
        _cb.apply(this, arguments)
      }
      const priority = args.pop()
      logger.info(`enqueue: ${method}(${this.$socket.id})[${this.queue.running()}/${this.queue.length()}]`)
      this.queue.push(() => this[method].apply(this, args), priority, cb)
    }
  }

  /**
   * 指定されたパスの投稿を取得する。
   * 
   * @param {string} slug - パス
   * @param {string} type - 親ディレクトリ
   * @param {Boolean} isDev - 下書き保存を所得するかどうか
   * @return {Promise<Object, Error>} 投稿
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  storyBySlug (slug, type, isDev) {
    if (!type) type = 'page'
    if (slug.substr(0, 1) !== '/') slug = `/${slug}`

    return this.$storyapi.get(`stories/${type}${slug}`, {
      version: isDev ? 'preview' : 'published'
    })
  }

  /**
   * 投稿を全て取得
   * 
   * @param {Object} params - パラメータ
   * @property {string} start_with - このパス以下の投稿を取得
   * @property {Boolean} version - 公開/下書き
   * @param {Boolean} isDev - 下書き保存を所得するかどうか
   * @return {Promise<Object, Error>} 投稿一覧
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  stories (params, isDev) {
    return this.$storyapi.get('stories/', {
      version: isDev ? 'preview' : 'published',
      ...params
    })
  }

  /**
   * アップロードされた画像を保存
   * @param {Object} info
   * @property {String} info.src DataURI文字列
   * @property {Number} info.key ID
   * @property {String} info.ext 拡張子
   * @return {Promise<Object, Error>} 画像情報
   * @since 0.0.1
   * @version 0.0.1
   */
  storeImage (info) {
    return require('./ffmpeg')
      .getInstance(this.$socket.id)
      .data2image(info)
  }

  /**
   * プレビュー動画を生成
   * @param {Array} images
   * @return {Promise<String, Error>} 動画のdataURI
   * @since 0.0.1
   * @version 0.0.1
   */
  makePreview (images) {
    return require('./ffmpeg')
      .getInstance(this.$socket.id)
      .buildPreview(images)
  }
}
