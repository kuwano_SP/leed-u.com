const consola = require('consola')
const logger = consola.withScope('socket.io:ffmpeg')
const cp = require('child_process')
const fs = require('fs')
const _ = require('underscore')
const ffmpeg = require('fluent-ffmpeg')
const { path, version } = require('@ffmpeg-installer/ffmpeg')
const axios = require('axios')
ffmpeg.setFfmpegPath(path)
ffmpeg.setFfprobePath(require('@ffprobe-installer/ffprobe').path)
logger.info(`ffmpeg version: ${version}`)

const SocketHandler = require('../handler')
const opt = {
  timeout: 60,
  logger: {
    debug: logger.debug,
    info: logger.info,
    warn: logger.warn,
    error: logger.error
  }
}
/**
 * ffmpegに関するクラス
 */
module.exports = class Ffmpeg {
  /**
   * dataURIから動画を保存する。
   * @since 0.0.1
   * @version 0.0.1
   */
  static getInstance (id) {
    if (!Ffmpeg.instances) Ffmpeg.instances = {}
    const map = Ffmpeg.instances
    if (id && !map[id]) {
      map[id] = new Ffmpeg(id)
    }
    return map[id]
  }
  /**
   * dataURIから動画を保存する。
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  constructor (id) {
    // トランジション動画の重複ダウンロードを避けるため
    this.transition = {}
    const base = '/tmp/'
    /**
     * 作業ディレクトリ
     * @type {String} パス文字列
     */
    this.cwd = base + id + '/'
    cp.execFile('rm', ['-rf', this.cwd], err => {
      if (err) throw err
      fs.mkdir(this.cwd, err => { if (err) throw err })
    }) // cp.execFile

    // たまると時間がかかるのでとりあえず無視
    /*
    // ６時間を超過した作業ディレクトリは削除
    const now = new Date()
    const idLength = require('base64id').generateId().length
    const list = fs.readdirSync(base).map(filename => {
      return { filename, mod: fs.statSync(base + filename).mtime }
    })
    for (const file of list) {
      if (now - file.mod > 1000 * 60 * 60 * 6 // 6時間を超過
      && file.filename.length === idLength) {
        cp.execFile('rm', [base + file.filename])
      }
    }
    */
  }
  /**
   * dataURIから動画を保存する。
   * @param {Object} image - 画像情報
   * @return {Promise<Object, Error>} 画像/動画情報
   * @since 0.0.1
   * @version 0.0.1
   */
  data2image (image) {
    const path = `${this.cwd}${image.key}.${image.ext}`
    const [meta, base64] = image.src.split(',')
    const decoded = Buffer.from(base64, 'base64')
    logger.debug(`save to ${path}`)

    return this._saveImageFromData(path, decoded)
      // トランジションのインポート
      .then(
        image.transition
          ? this._importTransition(image)
          : path => Promise.resolve(path)
      )
      // シーンの保存
      .then(this._saveScene(image))
      .then(() => Promise.resolve(path))
  }
  /**
   * プレビュー動画の生成
   * @param {...Object} images - 画像/動画情報の配列
   * @return {Promise<Object, Error>} 画像/動画情報
   * @since 0.0.1
   * @version 0.0.1
   */
  buildPreview (images) {
    const meta = this.transition.meta
    const cmd = ffmpeg(opt)
      .videoCodec('libx264')
    const transitions = []
    const filter = []
    let prev = null
    _.each(images, (image, i) => {
      const getFile = (ext) => image.name.replace(image.name.split('.').pop(), ext)
      cmd.input(getFile('mp4'))
      if (prev && image.transition) {
        const [tp, end] = image.point.split('/')
        const tprev = end - tp
        // トランジション動画の挿入
        filter.push({
          filter: [
            'movie=' + getFile('mov'),
            'scale=' + meta.width + ':' + meta.height,
            'setpts=PTS-STARTPTS'
          ],
          outputs: `t${i}base`
        })
        // オーバーレイ用と基礎（前半・後半）
        filter.push({
          inputs: `t${i}base`,
          filter: 'split',
          options: 3,
          outputs: [`t${i}back1`, `t${i}back2`, `t${i}front`]
        })
        // 基礎（前半・後半）としてトリミング
        filter.push({
          inputs: `t${i}back1`,
          filter: 'trim',
          options: { end_frame: tp },
          outputs: `t${i}back1trimed`
        })
        filter.push({
          inputs: `t${i}back2`,
          filter: 'trim',
          options: {
            start_frame: parseInt(tp) + 1,
            end_frame: end
          },
          outputs: `t${i}back2trimed`
        })
        filter.push({
          inputs: `t${i}back2trimed`,
          filter: 'setpts',
          options: 'PTS-STARTPTS',
          outputs: `t${i}back2trimed`
        })
        // 前半の動画
        filter.push({
          inputs: i - 1 + ':v',
          filter: [
            'trim=start_frame=' + (meta.cof(prev.duration) - tprev)
            + ':end_frame=' + meta.cof(prev.duration),
            'setpts=PTS-STARTPTS'
          ],
          outputs: `t${i}back1over`
        })
        filter.push({
          inputs: [`t${i}back1trimed`, `t${i}back1over`],
          filter: 'overlay',
          outputs: `t${i}back1r`
        })
        // 後半の動画
        filter.push({
          inputs: i + ':v',
          filter: [
            'trim=start_frame=0:end_frame=' + tp,
            'setpts=PTS-STARTPTS'
          ],
          outputs: `t${i}back2over`
        })
        filter.push({
          inputs: [`t${i}back2trimed`, `t${i}back2over`],
          filter: 'overlay',
          outputs: `t${i}back2r`
        })
        // 基礎の結合
        filter.push({
          inputs: [`t${i}back1r`, `t${i}back2r`],
          filter: 'concat',
          options: { n: 2, v: 1, a: 0 },
          outputs: `t${i}back`
        })
        // 基礎にオーバーレイ
        filter.push({
          inputs: [`t${i}back`, `t${i}front`],
          filter: 'overlay',
          outputs: `t${i}`
        })
        // 前の動画と結合する
        filter.push({
          inputs: i - 1 + ':v',
          filter: [
            `trim=start_frame=${i === 1 ? 0 : tp}:end_frame=${meta.cof(prev.duration) - tprev}`,
            'setpts=PTS-STARTPTS'
          ],
          outputs: `b${i}v`
        })
        // 前の動画と結合する
        filter.push({
          inputs: [`b${i}v`, `t${i}`],
          filter: 'concat',
          options: { n: 2, v: 1, a: 0 },
          outputs: `t${i}w`
        })
        transitions.push(`t${i}w`)
      }
      prev = image
    })

    const out = this.cwd + 'edit.mp4'
    const duration = images[images.length - 1].duration
    const [tp, end] = images[images.length - 1].point.split('/')
    const withLogo = 'v+logo'
    transitions.push(withLogo)
    return this._save(cmd.complexFilter([
      ...filter, {
        // 最後の動画を分ける（トランジション部分とそうでない部分）
        inputs: images.length - 1 + ':v',
        filter: 'split',
        outputs: ['main1', 'main2']
      }, {
        inputs: 'main1',
        filter: [
          `trim=start_frame=${tp}:end_frame=${meta.cof(duration) - 10}`,
          'setpts=PTS-STARTPTS'
        ],
        outputs: 'main_s'
      }, {
        inputs: 'main2',
        filter: 'trim',
        options: {
          start_frame: meta.cof(duration) - 10,
          end_frame: meta.cof(duration)
        },
        outputs: 'main2'
      }, {
        inputs: 'main2',
        filter: 'setpts',
        options: 'PTS-STARTPTS',
        outputs: 'main_t'
      }, {
        // ロゴ動画の読み込み
        filter: [
          'movie=' + process.cwd() + '/src/static/logo.mp4',
          'scale=' + meta.width + ':-1',
          'setpts=PTS-STARTPTS'
        ],
        outputs: 'logo'
      }, {
        // 動画を分ける（トランジション部分とそうでない部分）
        inputs: 'logo',
        filter: 'split',
        outputs: ['logo1', 'logo2']
      }, {
        inputs: 'logo1',
        filter: 'trim',
        options: { end_frame: 9 },
        outputs: 'logo_t'
      }, {
        inputs: 'logo2',
        filter: 'trim',
        options: { start_frame: 10 },
        outputs: 'logo2'
      }, {
        inputs: 'logo2',
        filter: 'setpts',
        options: 'PTS-STARTPTS',
        outputs: 'logo_s'
      }, {
        // トランジションを設定
        inputs: ['main_t', 'logo_t'],
        filter: 'overlay',
        options: { y: 'h-(n*h/10)' },
        outputs: 'transition'
      }, {
        // トランジションを結合
        inputs: ['main_s', 'transition', 'logo_s'],
        filter: 'concat',
        options: { n: 3, v: 1, a: 0 },
        outputs: withLogo
      }, {
        // 全ての動画を繋げて完成
        inputs: transitions,
        filter: 'concat',
        options: { n: transitions.length, v: 1, a: 0 },
        outputs: 'out'
      }
    ], 'out'), out)
      .then(() => new Promise((resolve, reject) => {
        fs.readFile(out, 'base64', (err, base64) => {
          if (err) return reject(err)
          resolve('data:video/mp4;base64,' + base64)
        })
      }))
  }
  
  /**
   * base64文字列を指定されたパスに保存する
   * @param {String} path - 保存するパス
   * @param {String} base64 - デコード文字列
   * @return {Promise<String, Error>} <出力ファイルのパス, エラー>
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  _saveImageFromData (path, base64) {
    return new Promise((resolve, reject) => {
      // 画像として保存
      fs.writeFile(path, base64, 'base64', err => err ?
        reject(err) : resolve(path))
    })
  }
  
  /**
   * トランジションのインポート
   * @param {Object} image - 素材情報
   * @return {Promise<String, Error>} <出力ファイルのパス, エラー>
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  _importTransition (image) {
    const transition = this.transition[image.transition]
    // ダウンロードしてUInt8array型のデータを保持する。
    if (!transition) return path => axios.get(`https:${image.transition}`, {
      responseType: 'arraybuffer'
    }).then(({ data }) => {
      const zipBuffer = Buffer.from(data, 'utf8')
      const NodeZip = require('node-zip')
      const zip = new NodeZip(zipBuffer.toString('base64'), { base64: true })
      this.transition[image.transition] = zip.files['sample/transition.mov']._data.getContent()
      logger.debug(`downloaded transition ${image.transition}`)
      return this._importTransition(image)(path)
    })
    // ダウンロード済みUInt8arrayからmov生成
    return path => new Promise((resolve, reject) => {
      const file = `${this.cwd}${image.key}.mov`
      // トランジションとして保存
      logger.debug(`Import ${image.transition} as ${file}`)
      fs.writeFile(file, new Buffer(transition), err => {
        err ? reject(err) : resolve(file)
      })
    })
    // トランジションのメタデータを保持
    .then(
      this.transition.meta
        ? () => Promise.resolve(path)
        : file => new Promise((resolve, reject) => {
          // トランジションのメタデータ
          ffmpeg.ffprobe(file, (err, meta) => {
            if (err) return reject(err)
            const stream = meta.streams[0]

            const width = 320
            const height = width * stream.width / stream.height
            const [numer, denom] = stream.avg_frame_rate.split('/')
            const fps = numer / denom
            this.transition.meta = {
              fps, cof (duration) {
                return Math.ceil(this.fps * duration)
              },
              ratio: stream.display_aspect_ratio,
              width,
              height,
              /**/
              // オリジナル
              width: stream.width,
              height: stream.height,
              /**/
            }
            resolve(path)
          })
        })
    )
  }

  /**
   * シーン動画を登録ffmpegを実行する
   * @param {Object} image - 素材情報
   * @return {function(): Promise<String, Error>} 出力ファイルのパス
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  _saveScene (image) {
    return path => {
      const meta = this.transition.meta
      const telopFilter = (flag) => {
        if (!image.text) return []
        const Telop = require('./telop/' + image.animation || '')
        const telop = new Telop(image, { ...meta }, flag)
        return telop.filter
      }
      const speed = meta.width / 24 // ピクセル/秒
      const mw = speed * image.duration
      const diff = mw / 2
      const w = meta.width + mw
      const cmd = ffmpeg(opt)
        .input(path)
        .complexFilter([
          {
            // 空の動画を設定
            filter: 'nullsrc',
            options: {
              size: `${meta.width}x${meta.height}`,
              // fps設定
              rate: meta.fps,
              duration: image.duration,
              sar: meta.ratio.replace(':', '/')
            },
            outputs: 'src'
          }, {
            // クロップするサイズより1pxずつ増やして拡大
            inputs: '0:v',
            filter: 'scale',
            options: {
              w: w + 1, h: meta.height + 1,
              force_original_aspect_ratio: 'increase'
            },
            outputs: 'scaled'
          }, {
            // 画像をクロップする
            inputs: 'scaled',
            filter: 'crop',
            options: {
              w, h: meta.height,
            },
            outputs: 'scaled'
          }, {
            inputs: ['src', 'scaled'],
            filter: 'overlay',
            options: {
              x: `(W-w)/2+t*(${mw}/${image.duration})-${diff}`,
            },
            outputs: 'video'
          }, ...telopFilter('video')
        ], 'out')
        // コーデック指定
        .videoCodec('libx264')
      return this._save(cmd, path.replace(path.split('.').pop(), 'mp4'))
    }
  }

  /**
   * ffmpegを実行する
   * @param {ffmpeg.cmd} cmd - 実行するクラス
   * @param {String} path - 保存先
   * @return {Promise<String, Error>} 出力ファイルのパス
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  _save (cmd, path) {
    const isDev = process.env.NODE_ENV !== 'production'
    return new Promise((resolve, reject) => {
      cmd.on('start', line => {
        line = '[spawned] '
          + line
            .split(' -')
            .join(' \\\n-')
            .split(';')
            .join('; \\\n')
        if (isDev) logger.debug(line)
      }).on('progress', progress => {
        const socket = SocketHandler.instance.$socket
        const current = path.split('/').pop().split('.')[0]
        const percent = progress.percent
        let max = 7175
        if (current === 'edit') max = 478
        const val = parseInt(percent / max * 100)
        socket.emit('ffmpeg/progress', percent ? val : -1, current)
        if (isDev)
          console.log('Processing: ' + percent + '% done', current);
      }).on('error', (err, stdout, stderr) => {
        logger.error('Faild processing', stdout, stderr);
        reject(err)
      }).on('end', logger.debug)
        .on('end', resolve).save(path)
    })
  }

  /**
   * URLからdestにダウンロード保存。
   * @param {String} url - ダウンロードするファイルのURL
   * @param {String} dest - 保存先のパス
   * @return {Promise<String, Error>} 保存先
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  _download (url, dest) {
    return new Promise((resolve, reject) => {
      const file = fs.createWriteStream(dest)
      const req = require('http').get(url, res => {
        res.pipe(file)
        file.on('finish', () => file.close(resolve))
        file.on('error', err => {
          file.unlink(dest)
          reject(err)
        })
      })
    })
  }
}