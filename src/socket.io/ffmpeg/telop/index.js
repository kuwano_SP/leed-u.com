const _ = require('underscore')

module.exports = class TelopAnimation {
  /**
   * 組み合わせを返す。
   * @param {Number} n - 
   * @return {Object} x,yの関数セット
   * @private
   * @since 0.2.4
   * @version 0.2.4
   */
  $nCr (n, r) {
    if ( r * 2 > n ) r = n - r
    let dividend = 1
    let divisor = 1
    for ( let i = 1; i <= r; ++i ) {
      dividend *= (n-i+1);
      divisor  *= i;
    }
    return dividend / divisor;
  }
  /**
   * ペジェ曲線のxとyの関数を返す。
   * @param {...Array} point - (x, y)のセット
   * @return {Object} x,yの関数セット
   * @private
   * @since 0.2.4
   * @version 0.2.4
   */
  $peje (...points) {
    points.unshift([0, 0])
    points.push([1, 1])
    const n = points.length - 1
    const calc = (i, point) => {
      const coefficient = this.$nCr(n, i)
      const [x, y] = point
      return {
        x: `${coefficient}*pow((1-t),${n-i})*pow(t,${n-(n-i)})*${x}`,
        y: `${coefficient}*pow((1-t),${n-i})*pow(t,${n-(n-i)})*${y}`
      }
    }
    let calced = { x: '', y: '' }
    _.each(points, (point, i) => {
      const p = calc(i, point)
      calced.x += p.x + (i === n ? '' : '+')
      calced.y += p.y + (i === n ? '' : '+')
    })
    return {
      x: t => calced.x.replace(/t/g, t),
      y: t => calced.y.replace(/t/g, t),
    }
  }
  /**
   * テロップに関するフィルタを返す。
   * @return {...ffmpeg.filter} フィルター
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  constructor (image, meta, flag) {
    _.each(image, (v, k) => this[k] = v)
    _.each(meta, (v, k) => this[k] = v)
    this.flag = flag
    const fontsize = this.fontsize || 16
    this.fontsize = fontsize * meta.height / 320
    this.backFrame = Math.ceil(this.fps * .5)
    if (this.key > 0) {
      const [tp, end] = this.point.split('/')
      this.delay = parseInt(end) - parseInt(tp)
    }
  }
  /**
   * テロップに関するフィルタを返す。
   * @return {...ffmpeg.filter} フィルター
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  prepareBackground () {
    const expr = frame => {
      const val = `if(gte(N,${frame}),1,N/${frame})`
      return `A*(${val})+B*(1-(${val}))`
    }
    return [
      {
        inputs: this.flag,
        filter: 'split',
        outputs: ['va', 'vb']
      }, {
        inputs: 'va',
        filter: 'drawbox',
        options: {
          color: (this.bg.color.replace('#', '0x') || 'black') + '@0.7',
          t: 'fill',
          x: 0,
          y: `ih-${this.fontsize * 5}`,
          w: 'iw',
          h: this.fontsize * 5
        },
        outputs: 'va'
      }, {
        inputs: ['va', 'vb'],
        filter: 'blend',
        options: {
          all_expr: this.delay
            ? `if(gte(N,${this.delay}),${expr(this.delay + this.backFrame)},A*0+B*1)`
            : expr(this.backFrame)
        },
        outputs: 'out'
      }
    ]
  }
  /**
   * テロップに関するフィルタを返す。
   * @return {...ffmpeg.filter} フィルター
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  telop () {
    const filter = []
    _.each(this.text.split(/\r\n|\r|\n/), (text, i) => {
      if (!text) return
      const y = `h-th*${3 / 2 * (3 - i)}`
      const duration = parseInt(this.fps * .8)
      // start x
      const sx = 'w/2'
      // end x
      const ex = this.fontsize / 2
      // alpha var
      const av = `1/pow(${ex}-w,2)*pow(x-w-${ex},2)`
      // (0<=t<=1)
      const _t = i + this.backFrame + (this.delay ? this.delay : 0)
      const t = `(n-${_t})/${duration}`
      // ease cubic beziler
      // const timing = this.$peje([.25, .1], [.25, 1]).y(t)
      const timing = this.$peje([.47, .8], [.45, .95]).y(t)
      const tx = `${sx}-(${sx}-${ex})*(${timing})`
      const x = `st(0,${t});if(between(ld(0),0,1),${tx},if(gt(ld(0),1),${ex},${sx}))`

      // text
      filter.push({
        inputs: 'out',
        filter: 'drawtext',
        options: {
          alpha: `st(0,${av});if(between(ld(0),0,1),ld(0),1)`,
          fontfile: `${process.cwd()}/lib/fonts/SG-Bold.otf`,
          fontsize: this.fontsize, x, y,
          fontcolor: this.font.color || 'white',
          text,
        },
        outputs: 'out'
      })
    })
    return filter
  }
  /**
   * テロップに関するフィルタを返す。
   * @return {...ffmpeg.filter} フィルター
   * @private
   * @since 0.0.1
   * @version 0.0.1
   */
  get filter () {
    return [
      ...this.prepareBackground(),
      ...this.telop()
    ]
  }
}