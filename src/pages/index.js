import Vue from 'vue'
import Component from 'vue-class-component'
import { Watch } from 'vue-property-decorator'
import _ from 'underscore'
import SelectTemplate from './.editor/SelectTemplate'
import UploadImages from './.editor/UploadImages'
import Preview from './.editor/Preview.vue'
let previewTimer

/**
 * 非同期データの取得
 * @param {NUXT.context} ctx - コンテキスト
 * @return {Promise<Object, Error>} vueの初期データ
 * @since 0.0.1
 * @version 0.0.1
 */
export const asyncData = ({ app, route, error }) => {
}

/**
 * ルートページasyncData
 * @param {NUXT.context} ctx - コンテキスト
 * @return {Promise<Object, Error>} vueの初期データ
 * @since 0.0.1
 * @version 0.0.1
 */
export function head () {
  return {
    title: '簡単動画作成',
    meta: [
      { hid: 'description', name: 'description', content: '簡単に動画が作れます。' }
    ]
  }
}

@Component({
  name: 'editor',
  components: {
    SelectTemplate,
    UploadImages,
    Preview
  },
  asyncData,
  head
})

/**
 * @return {Vue.meta} HTMLヘッド
 * @since 0.1.1
 * @version 0.1.1
 */
export default class extends Vue {
  /**
   * @return {Vue.meta} HTMLヘッド
   * @since 0.1.1
   * @version 0.1.1
   */
  currentStep = 1
  /**
   * @return {Vue.meta} HTMLヘッド
   * @since 0.1.1
   * @version 0.1.1
   */
  steps = [
    {
      editable: true,
      component: 'select-template',
      text: 'テンプレート'
    }, {
      editable: true,
      component: 'upload-images',
      text: '画像/動画'
    }, {
      component: 'preview',
      text: '動画完成'
    },
  ]
  /**
   * @return {Vue.meta} HTMLヘッド
   * @since 0.1.1
   * @version 0.1.1
   */
  content = {
    images: [],
    template: null
  }
  /**
   * @return {Vue.meta} HTMLヘッド
   * @since 0.1.1
   * @version 0.1.1
   */
  get stepLen () {
    return this.steps.length
  }
  
  /**
   * @return {Vue.meta} HTMLヘッド
   * @since 0.1.1
   * @version 0.1.1
   */
  get stepFlag () { return this.currentStep }
  set stepFlag (b) {
    if (b && this.currentStep < this.stepLen) this.currentStep++
    else if (!b && this.currentStep > 1) this.currentStep--
    else console.log('nothing to do', this.currentStep, b)
  }
  @Watch('content.template')
  onTemplateChange ({ content, meta_data, name }) {
    this.content.name = name
    this.content.images = content.scenes
    let i = 0
    for (let image of this.content.images) {
      image.key = i++
    }
  }
  /**
   * @return {Boolean} プレビュー工程への準備完了しているかどうか
   * @since 0.1.1
   * @version 0.1.1
   */
  get isReady () {
    if (this.content.images.length === 0) return false
    let flag = false
    flag = _.some(this.content.images, image => {
      return image.video !== true
    })
    return !flag
  }
}