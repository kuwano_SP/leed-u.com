# ページ

This directory contains your Application Views and Routes.
The framework reads all the .vue files inside this directory and create the router of your application.

More information about the usage of this directory in the documentation:
https://nuxtjs.org/guide/routing

## [/]

### [`_.vue`]

`this.$route.path`の値によって振り分けられる。
`path`の情報を元にstoryblokから投稿を所得して表示


### [`editor.vue`]

![完成イメージ](/manual/asset/editor/model.jpg)
![広告動画作成ページ](/manual/asset/editor/sample.jpg)
強みはURLを入力してそこから動画が作れる

ユーザーが画像/動画をアップロードして、テロップを入力するだけで広告で利用するような動画のプレビューができる。

1. テンプレートを選択する。
1. 画像/動画をテンプレートが指定する枚数アップロードする。
1. 自動でプレビュー動画の生成開始
1. 生成が完了したら、画面に表示（自由にダウンロード可能）
1. 気に入ったら、高画質で動画を作れるように。*（未実装）*

#### *@problem* テンプレートを選択するステップを後に回したい

*理由*: ユーザーに強制するステップ数を減らしたいから。
*対応*: テンプレート一覧読み込み時に一番上のテンプレートを選択状態にする。

#### *@todo* テンプレートの読み込みをasyncDataに取り込む

テンプレートの読み込みに時間がかかる。それまでに画像をアップロードするとエラーになるので。