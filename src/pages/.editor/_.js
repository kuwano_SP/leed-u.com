import Vue from 'vue'
import Component from 'nuxt-class-component'
import Page from '@/components/ThePage'
/**
 * @external {Vue.meta} https://ja.nuxtjs.org/guide/views#html-ヘッド
 */

/**
 * ルートページasyncData
 * @param {NUXT.context} ctx - コンテキスト
 * @return {Promise<Object, Error>} vueの初期データ
 * @since 0.0.1
 * @version 0.0.1
 */
export const asyncData = ({ app, route, error }) => {
  const path = route.path
  // 無駄なAPI通信を避けるため
  if (path.indexOf('/src/') !== -1
  || path.indexOf('/_nuxt/') !== -1) return Promise.resolve({})
  return app.$socket.do('storyBySlug', path)
    .catch(err => {
      error({ statusCode: 404, message: err.message })
    })
}

/**
 * @return {Vue.meta} HTMLヘッド
 * @since 0.0.1
 * @version 0.0.1
 */
export const head = function () {
  const _meta = this.story.content.meta
  if (!_meta) return {}
  const metas = []
  for (const key of Object.keys(_meta)) {
    const content = _meta[key]
    const isProp = key.indexOf('_') !== -1
    const meta = {}

    if (isProp) {
      const property = key.replace('_', ':')
      meta.hid = key
      meta.property = property || meta[key.split(':')[1]]
    } else {
      meta.hid = key
      meta.name = key
    }
    meta.content = content
    metas.push(meta)
  }
  metas.push({
    property: 'og:type',
    content: this.$route.path === '/' ? 'website' : 'article'
  })
  return {
    title: _meta.title,
    meta: [
      ...metas
    ]
  }
}

/**
 * ルートインデックスページ
 * @since 0.0.1
 * @version 0.0.1
 */
@Component({
  name: 'storyblok',
  components: {
    Page
  },
  asyncData,
  head
})
export default class extends Vue {
  /**
   * @type {Object} Storyblok story
   * @since 0.0.1
   * @version 0.0.1
   */
  story = { content: {} }
}
