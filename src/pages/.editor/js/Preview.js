import Vue from 'vue'
import Component from 'vue-class-component'
import { Watch } from 'vue-property-decorator'
import _ from 'underscore'

@Component({
  name: 'preview-handler',
  props: {
    material: Object,
    next: Number,
    isReady: Boolean
  }
})

/**
 * @return {Vue.meta} HTMLヘッド
 * @since 0.1.1
 * @version 0.1.1
 */
export default class extends Vue {
  preview = ''
  firstTime = true
  progress = 0
  get message () {
    if (this.progress) {
      return '作成中（' + this.progress + '%）'
    } else {
      return 'サーバーの状況を確認中です。<br>しばらくしてもバーが動かない場合はサーバーが混雑しています。'
    }
  }
  @Watch('isReady')
  onReadyStateChanged () {
    if (this.isReady && this.next === 2 && this.firstTime) {
      this.$emit('update:next', true)
      this.firstTime = false
    }
  }
  
  @Watch('next')
  onStepChange (v) {
    if (this.isReady && this.next === 3) this.makePreview()
  }

  makePreview () {
    this.preview = ''
    this.progress = 0
    this.$socket.do('makePreview', this.material.images).then(data => {
      this.preview = data
    }).catch(err => {
      console.error(err)
      return Promise.reject(err)
    })
  }
  
  mounted () {
    this.$socket.on('ffmpeg/progress', (percent, file) => {
      if (file === 'edit' && percent > 0)
        this.progress = percent
    })
  }
}