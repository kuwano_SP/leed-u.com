import Vue from 'vue'
import Component from 'vue-class-component'
import { Watch } from 'vue-property-decorator'
import _ from 'underscore'

@Component({
  name: 'template-selector',
  props: {
    material: Object,
    next: Number
  }
})

/**
 * @return {Vue.meta} HTMLヘッド
 * @since 0.1.1
 * @version 0.1.1
 */
export default class extends Vue {
  /**
   * @type Number 読み込むページ
   * @since 0.1.1
   * @version 0.1.1
   */
  currentPage = 0
  /**
   * @type Boolean 次のページを読み込む必要があるかどうか
   * @since 0.1.1
   * @version 0.1.1
   */
  hasNextPage = true
  /**
   * @type Boolean テンプレート読み込み中かどうか
   * @since 0.1.1
   * @version 0.1.1
   */
  loading = true
  /**
   * @type Number テンプレートのインデックス
   * @since 0.1.2
   * @version 0.1.2
   */
  selected = -1
  /**
   * @type Array 読み込んだテンプレートたち
   * @since 0.1.1
   * @version 0.1.1
   */
  stories = []
  /**
   * @type Object 表示される通知
   * @since 0.1.1
   * @version 0.1.1
   */
  notice = {
    type: 'info',
    value: true,
    msg: 'テンプレートを読み込んでいます。'
  }
  /**
   * テンプレートリストを10ずつ取得。
   * @return Promise<stories, Error> 読み込まれたテンプレート
   * @since 0.1.1
   * @version 0.1.1
   */
  getTemplates () {
    if (!this.hasNextPage) return Promise.resolve(false)
    this.loading = true

    return this.$socket.do('stories', {
      starts_with: 'template',
      per_page: 10,
      page: ++this.currentPage
    }).then(({ stories }) => {
      this.hasNextPage = stories === 10
      this.loading = false
      this.stories = this.stories.concat(stories)
      this.notice.msg = '使用したいテンプレートをクリックしてください。'
    }).catch(console.error)
  }
  /**
   * 選択しているテンプレートの切り替え
   * @param {Number} index - テンプレート一覧のインデックス
   * @param {Object} ev - イベント
   * @since 0.1.1
   * @version 0.1.1
   */
  updateSelected (index, ev) {
    this.material.template = this.stories[index]
    this.selected = index
    this.$emit('update:next', true)
    if (ev) ev.currentTarget.checked = false
  }
  /**
   * 初めは初めに読み込まれたテンプレートを選択状態にする
   * @since 0.1.1
   * @version 0.1.1
   */
  mounted () {
    this.getTemplates()
  }
}