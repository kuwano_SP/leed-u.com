import Vue from 'vue'
import Component from 'vue-class-component'
import { Watch } from 'vue-property-decorator'
import _ from 'underscore'
import Draggable from 'vuedraggable'

const charcount = function (str) {
  let len = 0;
  str = escape(str);
  for (let i=0;i<str.length;i++,len++) {
    if (str.charAt(i) == "%") {
      if (str.charAt(++i) == "u") {
        i += 3;
        len++;
      }
      i++;
    }
  }
  return len;
}

@Component({
  name: 'image-uploader',
  components: { Draggable },
  props: {
    material: Object,
    next: Number,
    isReady: Boolean,
  }
})

/**
 * @return {Vue.meta} HTMLヘッド
 * @since 0.1.1
 * @version 0.1.1
 */
export default class extends Vue {
  /**
   * @type Boolean ヘルプを表示するかどうか
   * @since 0.1.1
   * @version 0.1.1
   */
  tooltipFlag = false
  /**
   * @type Number 画像のインデックス（指定しないときは-1）
   * @since 0.1.1
   * @version 0.1.1
   */
  inputIndex = -1
  /**
   * @type Boolean 入力ダイアログの表示/非表示
   * @since 0.1.1
   * @version 0.1.1
   */
  inputDialog = false
  /**
   * @type String 入力されたテキスト
   * @since 0.1.1
   * @version 0.1.1
   */
  inputText = ''
  /**
   * @type Array 入力テキストのルール
   * @since 0.1.1
   * @version 0.1.1
   */
  inputRules = [
    text => {
      if (typeof text !== 'string') return 'テロップが入力されていません。'
      const arr = text.split(/\r\n|\r|\n/)
      let flag = true
      if (arr.length > 3) return 'テロップに入力できるのは３行までです。'

      _.each(arr, line => {
        if (!flag) return
        flag = charcount(line) <= 30
      })
      return flag || 'いずれかの行で字数がオーバーしています。'
    }
  ]
  /**
   * @type Number ドラッグ中を検知
   * @since 0.1.1
   * @version 0.1.1
   */
  droppingFlag = false
  /**
   * @type Boolean vuedraggableのフラグ
   * @since 0.1.1
   * @version 0.1.1
   */
  draggableFlag = false
  /**
   * ドラッグ中を検知
   * @return {Boolean} ドラッグ中かどうか
   * @since 0.1.1
   * @version 0.1.1
   */
  get dropping () {
    return this.droppingFlag
  }
  /**
   * ドラッグ中状態の変更
   * @param {event} mouseEvent - mouseEvent
   * @since 0.1.1
   * @version 0.1.1
   */
  set dropping (mouseEvent) {
    switch (mouseEvent.type) {
      // フラグのon/offを切り替える
      case 'dragover':
      case 'dragleave':
        if (this.droppingFlag === (mouseEvent.type === 'dragover')) return
        // draggableでも反応するのを回避する
        if (!this.droppingFlag && this.draggableFlag) return
        this.droppingFlag = !this.droppingFlag
        break

      // ファイルをリストに追加する
      case 'drop':
        if (!this.droppingFlag) return
        const files = mouseEvent.dataTransfer.files
        this.droppingFlag = false
        for (let i = 0, l = files.length; i < l; i++) {
          if (i < this.limit) this.setImage(i, files[i])
        }
        break

      // 他のイベントには反応しない
      default:
        console.error('不明な操作が行われました。', mouseEvent.type)
    }
  }

  /**
   * <inpu type="file">にファイルが渡された時の処理
   *
   * @param {Event} e
   * @since 0.1.1
   * @version 0.1.1
   */
  inputFiles (e) {
    const files = e.target.files
    const index = e.currentTarget.index

    for (let i = 0, l = files.length; i < l; i++) {
      if (i + index < this.material.images.length) {
        this.setImage(i + index, files[i])
      }
    }
  }
  /**
   * ファイルをリストに追加する
   *
   * @param {Number} index
   * @param {File} file
   * @since 0.1.1
   * @version 0.1.1
   */
  setImage (index, file) {
    const reader = new FileReader()
    reader.onload = e => {
      const _file = {
        src: e.target.result,
        ext: file.name.split('.').pop()
      }
      this.$socket.do('storeImage', {
        ...this.material.images[index],
        ..._file
      }).then(name => {
        this.$set(this.material.images, index, {
          ...this.material.images[index],
          ..._file, ...{ video: true, name }
        })
      }).catch(err => {
        this.$set(this.material.images[index], 'src', '')
        this.$set(this.material.images[index], 'video', false)
        return Promise.reject(err)
      })
      this.$refs.input.index = undefined
    }
    reader.readAsDataURL(file)
    // loading gif
    // @see https://tenor.com/view/loading-gif-9856796
    this.$set(this.material.images[index], 'src', -1)
    this.$set(this.material.images[index], 'video', false)
  }
  /**
   * Drag&Dropで順番が変わった時
   * @since 0.1.1
   * @version 0.1.1
   */
  elementsChanged (type) {
    const { added, removed, moved } = type
    if (moved) {
      _.each(this.material.images, (image, i) => {
        for (let key of Object.keys(image)) {
          if (['src', 'name', 'text', 'key'].indexOf(key) !== -1) continue
          const value =  this.material.template.content.scenes[i][key]
          if (value) this.material.images[i][key] = value
        }
      })
      console.log(this.material.images)
    }
    if (added) {
      console.log('added', added)
    }
    if (removed) {
      console.log('removed', removed)
    }
  }

  /**
   * Drag&Dropで順番が変わった時
   * @since 0.1.1
   * @version 0.1.1
   */
  updateFlag () {
    const els = this.images
    const flag = this.info.template && !_.find(els, el => !el.video)
    this.$set(this.info, 'flag', flag ? els : flag)
  }

  /**
   * Drag&Dropで順番が変わった時
   * @since 0.1.1
   * @version 0.1.1
   */
  uploadTo (index) {
    if (!this.$refs.input) return console.warn('input準備中')
    this.$refs.input.index = index
    return this.$refs.input.click()
  }

  /**
   * Drag&Dropで順番が変わった時
   * @since 0.1.1
   * @version 0.1.1
   */
  @Watch('images')
  onChangeImages () {
    this.updateFlag()
  }
  /**
   * 入力ダイアログをインデックスを指定して開く
   * @since 0.1.1
   * @version 0.1.1
   */
  @Watch('inputIndex')
  onInputIndexChanged () {
    if (this.inputIndex < 0) return
    this.inputText = this.material.images[this.inputIndex].text
    this.inputDialog = true
  }
  /**
   * 入力テキスト素材に格納する
   * @since 0.1.1
   * @version 0.1.1
   */
  @Watch('inputDialog')
  onInputDialogStateChanged () {
    if (typeof this.inputDialog === 'number') {
      if (this.inputText)
        this.material.images[this.inputDialog].text = this.inputText
      const index = this.inputDialog
      const src = this.material.images[index].src
      if (!src) return console.log('not src')
      this.$set(this.material.images[index], 'video', false)
      this.$set(this.material.images[index], 'src', -1)
      this.$socket.do('storeImage', { ...this.material.images[index], src })
        .then(name => {
          this.$set(this.material.images, index, {
            ...this.material.images[index],
            ...{ video: true, src }
          })
        }).catch(err => {
          this.$set(this.material.images[index], 'src', '')
          this.$set(this.material.images[index], 'video', false)
          return Promise.reject(err)
        })
      this.inputDialog = false
    } else if (!this.inputDialog) {
      // 同じインデックスを再選択できないため
      this.inputIndex = -1
    }
  }
  
  mounted () {
    this.$socket.on('ffmpeg/progress', (progress, index) => {
      if (this.material.images[index]) {
        this.$set(this.material.images[index], 'src', progress)
      }
    })
  }
}