# コンポーネント

## 命名規則

基本は、`L{Type}{Name}`

ただし投稿タイプのコンポーネントは`The{PostType}`とする。


The components directory contains your Vue.js Components.
Nuxt.js doesn't supercharge these components.

**This directory is not required, you can delete it if you don't want to use it.**
