import Vue from 'vue'
import Component from 'vue-class-component'
import { Watch } from 'vue-property-decorator'

import _ from 'underscore'
import Draggable from 'vuedraggable'

/**
 *
 * @example
 * @Component({
 *  name: 'image-uploader',
 *  components: {
 *    Draggable,
 *  },
 *  props: {
 *    limit: Number,
 *    images: Object
 *  }
 * })
 */
@Component({
  name: 'LImageUploader',
  components: {
    Draggable,
  },
  props: {
    limit: Number,
    images: Array,
    info: Object
  }
})
export default class extends Vue {
  /**
   *
   */
  droppingFlag = false
  /**
   *
   */
  draggableFlag = false
  /**
   *
   */
  get dropping () {
    return this.droppingFlag
  }
  /**
   *
   */
  set dropping (mouseEvent) {
    switch (mouseEvent.type) {
      // フラグのon/offを切り替える
      case 'dragover':
      case 'dragleave':
        if (this.droppingFlag === (mouseEvent.type === 'dragover')) return
        // draggableでも反応するのを回避する
        if (!this.droppingFlag && this.draggableFlag) return
        this.droppingFlag = !this.droppingFlag
        break

      // ファイルをリストに追加する
      case 'drop':
        if (!this.droppingFlag) return
        const files = mouseEvent.dataTransfer.files
        this.droppingFlag = false
        for (let i = 0, l = files.length; i < l; i++) {
          if (i < this.limit) this.setImage(i, files[i])
        }
        break

      // 他のイベントには反応しない
      default:
        console.error('不明な操作が行われました。', mouseEvent.type)
    }
  }
  /**
   * <inpu type="file">にファイルが渡された時の処理
   *
   * @param {Event} e
   */
  inputFiles (e) {
    const files = e.target.files
    const index = e.currentTarget.index

    for (let i = 0, l = files.length; i < l; i++) {
      if (i + index < this.limit) this.setImage(i + index, files[i])
    }
  }
  /**
   * ファイルをリストに追加する
   *
   * @param {Number} index
   * @param {File} file
   */
  setImage (index, file) {
    const reader = new FileReader()
    reader.onload = e => {
      file.src = e.target.result
      file.key = index
      file.ext = file.name.split('.').pop()
      const _file = JSON.parse(JSON.stringify(file))
      this.$socket.do('storeImage', JSON.parse(JSON.stringify(file)))
        .then(() => this.$set(this.images, index, {
          ...this.images[index],
          ...file,
          video: true
        })).catch(err => {
          console.error(err)
          this.$set(this.images[index], 'src', '')
          return Promise.reject(err)
        })
      this.$refs.input.index = undefined
    }
    reader.readAsDataURL(file)
    this.$set(this.images[index], 'src', 'loading')
  }
  /**
   * Drag&Dropで順番が変わった時
   */
  elementsChanged (type) {
    const { added, removed, moved } = type
    if (moved) {
    }
    if (added) {
      console.log('added', added)
    }
    if (removed) {
      console.log('removed', removed)
    }
  }

  updateFlag () {
    const els = this.images
    const flag = this.info.template && !_.find(els, el => !el.video)
    this.$set(this.info, 'flag', flag ? els : flag)
  }

  uploadTo (index) {
    if (!this.$refs.input) return console.warn('準備中')
    this.$refs.input.index = index
    return this.$refs.input.click()
  }

  @Watch('template')
  onChangeTemplate () {
    this.updateFlag()
  }

  @Watch('images')
  onChangeImages () {
    this.updateFlag()
  }
}
