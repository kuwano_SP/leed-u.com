import Vue from 'vue'
import Component from 'vue-class-component'

@Component({
  name: 'LTemplateSelecter',
  props: {
    selected: Object
  }
})
export default class extends Vue {
  show = false
  stories = []
  loadable = true
  currentPage = 0

  getTemplates () {
    return this.$socket.do('stories', {
      starts_with: 'template',
      per_page: 10,
      page: ++this.currentPage
    }).then(({ stories }) => {
      if (stories.length < 10) this.loadable = false
      this.stories = this.stories.concat(stories)
      return Promise.resolve(stories)
    }).catch(console.err)
  }

  updateSelected (index) {
    this.$emit('update:selected', this.stories[index])
  }

  created () {
    this.getTemplates()
      .then(stories => this.updateSelected(0))
  }
}