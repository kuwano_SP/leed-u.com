import Vue from 'vue'
import Component from 'nuxt-class-component'
import Teaser from '@/components/LTeaser'
import Grid from '@/components/LGrid'
// import Alert from '@/components/body/alert'
// import BannerSection from '@/components/body/banner-section'
const components = {
  Teaser,
  Grid
//   Alert,
//  BannerSection
}

@Component({
  name: 'ThePage',
  components,
  props: ['blok'],
})

export default class extends Vue {
}