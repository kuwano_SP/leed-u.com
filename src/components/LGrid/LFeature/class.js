import Vue from 'vue'
import Component from 'vue-class-component'

@Component({
  name: 'LFeature',
  props: {
    blok: Object,
    flexInfo: Array
  }
})
export default class extends Vue {
  get flex () {
    const [l, i] = this.$props.flexInfo
    const model = { xs12: true }

    model.sm6 = l % 2 === 0
    model.sm4 = l % 3 === 0
    model.md3 = l % 4 === 0
    model.md2 = l % 6 === 0

    if (l % 5 === 0) {
      model.sm6 = true
      model['offset-sm-1'] = i % 2 !== 0
    }
    return model
  }
}
