import Vue from 'vue'
import Component from 'vue-class-component'
import Feature from './LFeature'
const components = {
  Feature
}

@Component({
  name: 'LGrid',
  components,
  props: {
    blok: Object
  }
})
export default class extends Vue {}
