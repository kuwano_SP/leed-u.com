/**
 * @external {Vuex.Store} https://vuex.vuejs.org/ja/api/#vuex-store-コンストラクタオプション
 */
/**
 * @external {Vuex.state} https://vuex.vuejs.org/ja/guide/state.html
 */
/**
 * @external {Vuex.mutations} https://vuex.vuejs.org/ja/guide/mutations.html
 */
/**
 * @external {Vuex.actions} https://vuex.vuejs.org/ja/guide/actions.html
 */
/**
 * @external {Vuex.getters} https://vuex.vuejs.org/ja/guide/getters.html
 */
/**
 * @external {NUXT.nuxtServerInit} https://ja.nuxtjs.org/guide/vuex-store#nuxtserverinit-アクション
 */

/**
 * ルートステイト
 *
 * @return {Vuex.state} ステイトオブジェクト
 * @property {Boolean} sidebar - サイドバーが表示中かどうか
 * @since 0.0.1
 * @version 0.0.1
 */
export const state = () => ({
  sidebar: false,
  notifications: []
})

/**
 * ルートミューテーション
 *
 * @type {Vuex.mutations}
 * @property {Function} toggleSidebar - サイドバーのON/OFFを切り替える
 * @example
 * // in layout/default.js
 * export default class {
 * //...
 *   created () { this.$store.commit('socketConnect') }
 * //...
 * }
 * @since 0.0.1
 * @version 0.0.1
 */
export const mutations = {
  toggleSidebar (state) {
    state.sidebar = !state.sidebar
  }
}

/**
 * ルートアクション
 *
 * @type {Vuex.actions}
 * @property {NUXT.nuxtServerInit} nuxtServerInit - ストアの初期化
 * @since 0.0.1
 * @version 0.0.1
 */
export const actions = {
  nuxtServerInit (store, ctx) {
  }
}

export const getters = {
  meta: (state, getters) => ({ content, slug }) => {
    const meta = content.meta
    const metas = []
    meta.forEach((content, key) => {
      const isProp = key.indexOf('_') !== -1
      const meta = {}

      if (isProp) {
        const property = key.replace('_', ':')
        meta.hid = key
        meta.property = property || meta[key.split(':')[1]]
      } else {
        meta.hid = key
        meta.name = key
      }
      meta.content = content
      metas.push(meta)
    })
    metas.push({ property: 'og:type', content: slug === 'page' ? 'website' : 'article' })
    return {
      title: meta.title,
      meta: [
        ...metas
      ]
    }
  },
  // eslint-disable-next-line camelcase
  link: (state, getters) => ({ cached_url, linktype, url }) => {
    switch (linktype) {
      case 'story':
        return cached_url.replace('page', '')

      case 'url':
        return url
    }
  }
}
