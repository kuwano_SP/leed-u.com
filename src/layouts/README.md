# レイアウト

This directory contains your Application Layouts.

More information about the usage of this directory in the documentation:
https://nuxtjs.org/guide/views#layouts

**This directory is not required, you can delete it if you don't want to use it.**

## 基本レイアウト[`default.vue`]

デフォルトレイアウト

## エラーページ[`error.vue`]

404や500などのエラーページ