import Vue from 'vue';
import Component from 'vue-class-component'
/**
 * デフォルトレイアウト
 * @since 0.0.1
 * @version 0.0.1
 */
@Component({
  name: 'default'
})
export default class LayoutDefault extends Vue {
  /**
   * ドロワーのON/OFF
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  title = 'Leed'
  /**
   * ドロワーのON/OFF
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  drawerShown = false
  /**
   * ドロワーのアイコン表示
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  miniVariant = false
  /**
   * ライトドロワーが右かどうか
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  right = false
  /**
   * ライトドロワーの表示
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  rightDrawer = false
  /**
   * リストアイテム
   * @type {Object}
   * @property {Function} socketConnect - ソケット接続してオブジェクトを保持する
   * @property {string} toggleSidebar - サイドバーのON/OFFを切り替える
   * @since 0.0.1
   * @version 0.0.1
   */
  items =  []
  /**
   * ライトドロワーの表示
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  snackbar = false
  /**
   * タイトル
   * @type {String}
   * @since 0.0.1
   * @version 0.0.1
   */
  notice = {}
  /**
   * ライトドロワーの表示
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  get drawer () {
    return this.drawerShown
    // return this.$vuetify.breakpoint.lgAndUp || this.drawerShown
  }
  /**
   * ライトドロワーの表示
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  set drawer (v) {
    this.drawerShown = v
  }
  /**
   * ライトドロワーの表示
   * @type {Boolean}
   * @since 0.0.1
   * @version 0.0.1
   */
  get secureMenu () {
    const common = [
      {
        title: '表示モードの切り替え',
        on: {
          click: () => this.$store.commit('toggleTheme')
        }
      }
    ]
    return this.$auth && this.$auth.loggedIn ? [
      {
        title: 'ログアウト',
        nuxt: true,
        to: '/user?logout'
      },
      ...common
    ] : [
      {
        title: 'ログイン',
        nuxt: true,
        to: '/user'
      },
      ...common
    ]
  }
  /**
   * 作成時
   * @since 0.0.1
   * @version 0.0.1
   */
  created () {
    // グローバルメニューを取得
    this.$socket.do('storyBySlug', 'primary-menu', 'global')
      .then(data => {
        this.items = data.story.content.items
      })
  }
}
